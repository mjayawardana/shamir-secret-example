package com.codahale.shamir;

import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class AlgorithemCheck {

	public static void main(String[] args) {
		final Scheme scheme = new Scheme(new SecureRandom(), 5, 3);
	    final byte[] secret = "hello there".getBytes(StandardCharsets.UTF_8);
	    final Map<Integer, byte[]> parts = scheme.split(secret);
	    
	    final Map<Integer, byte[]> parts2 = new HashMap<Integer, byte[]>();
	    parts2.put(1, parts.get(1));
	    parts2.put(5, parts.get(5));
	    parts2.put(2, parts.get(2));
	    parts2.put(3, parts.get(3));

	    final byte[] recovered = scheme.join(parts2);
	    
	    
	    System.out.println(new String(recovered, StandardCharsets.UTF_8));
	    
	    shareTree();
	}
	
	public static void shareTree() {
	    final byte[] secret = "this is a secret".getBytes(StandardCharsets.UTF_8);
	    
	    // tier 1 of the tree
	    final Scheme adminScheme = new Scheme(new SecureRandom(), 5, 2);
	    final Map<Integer, byte[]> admins = adminScheme.split(secret);

	    // tier 2 of the tree
	    final Scheme userScheme = new Scheme(new SecureRandom(), 4, 3);
	    final Map<Integer, Map<Integer, byte[]>> users =  admins.entrySet()
	            .stream()
	            .collect(Collectors.toMap(Map.Entry::getKey, e -> userScheme.split(e.getValue())));
	    
	    System.out.println("Admin shares:");
	    System.out.printf("%d = %s\n", 1, Arrays.toString(admins.get(1)));
	    System.out.printf("%d = %s\n", 2, Arrays.toString(admins.get(2)));

	    System.out.println("User shares:");
	    System.out.printf("%d = %s\n", 1, Arrays.toString(users.get(1).get(1)));
	    System.out.printf("%d = %s\n", 2, Arrays.toString(users.get(3).get(2)));
	    System.out.printf("%d = %s\n", 3, Arrays.toString(users.get(3).get(3)));
	    System.out.printf("%d = %s\n", 4, Arrays.toString(users.get(3).get(4)));
	    
	    
	    final Map<Integer, byte[]> parts2 = new HashMap<Integer, byte[]>();
	    final Map<Integer, byte[]> parts3 = new HashMap<Integer, byte[]>();


	    
	    parts3.put(1, users.get(5).get(1));
	    parts3.put(2, users.get(5).get(2));
	    parts3.put(3, users.get(5).get(3));
	    parts3.put(4, users.get(5).get(4));

	    final byte[] recoveredUsers = userScheme.join(parts3);

	    Map<Integer,  byte[]> test = new HashMap<>();
	    test.put(3, admins.get(3));
	   // test.put(2, admins.get(2));
	    test.put(5, recoveredUsers);

//	    parts2.put(2, parts.get(2));
//	    parts2.put(3, users.get(3).to);

	    final byte[] recovered = adminScheme.join(test);
	    
	    
	    System.out.println(new String(recovered, StandardCharsets.UTF_8));

	    
	  }
}
